<Goals>
	<Drink>
		<HouseWithWater>
			<Pre>
				<Condition>Owns_House</Condition>
				<Condition>Does_House_Have_Water</Condition>
			</Pre>
			<Tasks>
				<ConditionalTask>Refill_House_Water_Storage</ConditionalTask>
				<Task>Go_To_Location(House)</Task>
				<Task>Drink_Water</Task>
			</Tasks>
		</HouseWithWater>
		<Well>
			<Pre>
				<Condition>Well_Exists</Condition>
			</Pre>
			<Tasks>
				<Task>Go_To_Location(Well)</Task>
				<Task>Use_Well</Task>
			</Tasks>
		</Well>
	</Drink>
	
	<Sleep>

	</Sleep>
	
	<Eat>

	</Eat>
</Goals>