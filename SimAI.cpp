#include <TL-Engine.h>	
#include <Windows.h>
#include "Source\mainSim.h"
using namespace tle;

void main()
{
	//Start Engine
	I3DEngine* tL = New3DEngine( kTLX );
	int screenX = GetSystemMetrics(SM_CXSCREEN);
	int screenY = GetSystemMetrics(SM_CYSCREEN);
	tL->StartFullscreen(screenX, screenY);
	//tL->StartWindowed();
	tL->SetWindowCaption("Project");
	tL->AddMediaFolder( "..\\SimAI\\Media" );

	//Set Settings
	MainSim* mS = new MainSim(tL);
	bool stopDrawing = false;

	while (tL->IsRunning())
	{
		float frameTime = tL->Timer();
		if (mS->Update(frameTime)) { break; };
		//if (tL->GetMouseX() > screenX || tL->GetMouseX() < 0 ||
		//	tL->GetMouseY() > screenY && tL->GetMouseY() < 0) {
		//	stopDrawing = true;
		//}
		//else {
		//	stopDrawing = false;
		//}
		//if (!stopDrawing) {
		//	tL->DrawScene();
		//}
		tL->DrawScene();
	}
	tL->Delete();
}
