#include "WorldGrid.h"

WorldGrid::WorldGrid()
{
	CreateGrid();
}

void WorldGrid::CreateGrid()
{
	for (int b = 0; b < startXLenght; b++) {
		mainGrid.push_back(CreateRow(b));
	}
}

int WorldGrid::GetXLenght()
{
	int x = 0;
	return mainGrid.size();
}

int WorldGrid::GetZLenght()
{
	int x = 0;
	return mainGrid[0].size();
}

float WorldGrid::GetGridPosX(int x, int z)
{
	return mainGrid[x][z].x;
}

float WorldGrid::GetGridPosY(int x, int z)
{
	return mainGrid[x][z].y;
}

float WorldGrid::GetGridPosZ(int x, int z)
{
	return mainGrid[x][z].z;
}

vector<WorldGrid::GridPos> WorldGrid::CreateColumn(int xGridPos)
{
	vector<GridPos> gridTemp; //Positions of new grid
	GridPos gridPos;
	for (int a = 0; a < upAmount; a++) {
		gridPos.x = xGridPos * gridSize;
		gridPos.z = (currentGridSizeDown + a) * gridSize;
		gridTemp.push_back(gridPos);
	}
	return gridTemp;
}

vector<WorldGrid::GridPos> WorldGrid::CreateRow(int zGridPos)
{
	vector<GridPos> gridRow;
	GridPos gridPos;
	for (int a = 0; a < sideAmount; a++) {
		gridPos.x = (currentGridLeft + a) * gridSize;
		gridPos.z = zGridPos * gridSize;
		gridRow.push_back(gridPos);
	}
	return gridRow;
}

void WorldGrid::AddRowTop()
{
	currentGridSizeUp++;
	upAmount++;
	mainGrid.push_back(CreateRow(currentGridSizeUp));
}

void WorldGrid::AddRowBottom()
{
	vector<vector<GridPos>> gridTemp;
	currentGridSizeDown--;
	upAmount++;
	gridTemp.push_back(CreateRow(currentGridSizeDown));
	for (int a = 0; a < mainGrid.size(); a++) {
		gridTemp.push_back(mainGrid[a]);
	}
	mainGrid.clear();
	mainGrid = gridTemp;
}

void WorldGrid::AddRowLeft()
{
	vector<GridPos> gridTemp;
	vector<GridPos> gridNewPos;
	currentGridLeft--;
	sideAmount++;
	gridNewPos = CreateColumn(currentGridLeft);
	for (int a = 0; a < gridNewPos.size(); a++) {
		mainGrid[a].insert(mainGrid[a].begin(), gridNewPos[a]);
	}
	int a = 0;
	//for (int a = 0; a < gridNewPos.size(); a++) {
	//	gridTemp.push_back(gridNewPos[a]);
	//	for (int b = 0; b < mainGrid[a].size(); b++) {
	//		gridTemp.push_back(mainGrid[a][b]);
	//	}
	//	mainGrid[a] = gridTemp;
	//	gridTemp.clear();
	//}
}

void WorldGrid::AddRowRight()
{
	vector<GridPos> gridNewPos;
	currentGridRight++;
	sideAmount++;
	gridNewPos = CreateColumn(currentGridRight);
	for (int a = 0; a < mainGrid.size(); a++) {
		mainGrid[a].push_back(gridNewPos[a]);
	}
}

void WorldGrid::ChangeTaken(int i, int x, int z)
{
	mainGrid[x][z].taken = i;
}
int WorldGrid::GetTaken(int x, int z)
{
	if (!(x < 0 || x > GetXLenght() - 1 || z < 0 || z > GetZLenght() - 1)) {
		return mainGrid[x][z].taken;
	}
}

void WorldGrid::ChangeScore(int value, int x, int z)
{
	mainGrid[x][z].score = value;
}

int WorldGrid::GetScore(int x, int z)
{
	return mainGrid[x][z].score;
}

vector<vector<WorldGrid::GridPos>> WorldGrid::GetGrid()
{
	return mainGrid;
}

