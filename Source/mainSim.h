#include <TL-Engine.h>	
#include <iostream>
#include "AgentManager.h"
#ifndef UICLASS_H
#define UICLASS_H
#include "UIClass.h"
#endif 
#include "MainMenu.h"
#ifndef PLACEABLEOJBJECTS_H
#define PLACEABLEOJBJECTS_H
#include "PlaceableObject.h"
#endif
#ifndef WORLDGRID_H
#define WORLDGRID_H
#include "WorldGrid.h"
#endif
#ifndef GAMEOBJECTS_H
#define GAMEOBJECTS_H
#include "GameObjects.h"
#endif
using namespace tle;

class MainSim
{

private:
	//Engine
	I3DEngine* tL;

	//Camera
	ICamera* camera;
	IMesh* cubeMesh;
	IModel* cameraBaseModel;
	float cameraSpeed = 50.0f;
	float cameraRotationSpeed = 1000.0f;
	bool mouseShown = true;
	bool cameraRotateLeft = false;
	bool cameraRotateRight = false;
	float cameraRotation = 45.0f;
	const float CAMERA_ROTATION_SPEED = 600.0F;
	float startRot = 0.0f;
	int zoomLevel = 10;
	int zoomScroll = 0;
	const float CAMERA_ZOOM_SPEED = 5.0f;

	ILight* light;
	ILight* amLight;

	//Skybox
	IMesh* skyboxMesh;
	IModel* skybox;

	//Floor
	IMesh* floorMesh;
	IModel* floor;

	//States
	enum Sim_States
	{
		Menu = 1,
		InSimulation = 2,
		Pause = 3,
		Loading = 4
	};
	int sim_State = Sim_States::Menu;
	//Classes
	AgentManager* agentManager;
	MainMenu* mainMenu;

	WorldGrid* worldGrid;
	PlaceableObject* placeableObjects;
	UIClass* uiClass;
	
	//Functions
	void UpdateCamera(float frameTime);

	void DebugControls();

	float timer = 0.0f;
	int uiCounter = 0;

	bool showArrow = false;
	bool showPath = false;
	int currentlyShownAgentPath = -1;
	bool showPathLoop = false;

	float timerTest = 0.0f;
	bool killTest = false;

	void LoadGame();
	void LoadObjects();


	//Passing from main menu
	int amountOfAgents;
	int agentStatMax;
	int agentStatMin;
	int agentMaxLife;

	int density = 30;

public:
	MainSim() {}
	MainSim(tle::I3DEngine*);
	~MainSim() {}
	bool Update(float frameTime);
};