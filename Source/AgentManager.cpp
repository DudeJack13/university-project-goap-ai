#include "AgentManager.h"

AgentManager::AgentManager(I3DEngine* tL, WorldGrid* wG, PlaceableObject* pO, UIClass* uI)
{
	this->tL = tL;
	agentMesh1 = tL->LoadMesh("dude.x");
	worldGrid = wG;
	placeableObjects = pO;
	uiClass = uI;
	arrowMesh = tL->LoadMesh("arrow.x");
	arrow = arrowMesh->CreateModel(0.0f,-10.0f,0.0f);
	arrow->SetSkin("red.PNG");
	arrow->RotateX(90.0f);
	pathCube = tL->LoadMesh("Cube.x");
}

int AgentManager::GetAgent(int i)
{
	for (int a = 0; a < agentClass.size(); a++) {
		if (agentClass[a]->GetID() == i) {
			return a;
		}
	}
}

int AgentManager::SpawnAgent(int gridX, int gridZ)
{
	agentClass.push_back(new Agent(
		tL, 
		worldGrid,
		placeableObjects,
		uiClass,
		agentMesh1, 
		agentIdCounter, 
		worldGrid->GetGridPosX(gridX, gridZ),
		worldGrid->GetGridPosY(gridX, gridZ) + AGENT_HEIGHT,
		worldGrid->GetGridPosZ(gridX, gridZ),
		gridX, 
		gridZ));
	agentIdCounter++;
	agentClass.back()->LoadComponents(worldGrid);
	//agentClass.back()->ChangeSkin("lava_rock.jpg");
	return agentIdCounter - 1;
}

int AgentManager::SpawnZombie(int gridX, int gridZ)
{
	int id = SpawnAgent(gridX, gridZ);
	agentClass[id]->TurnToZombie();
	agentClass[id]->ChangeSkin("lava_rock.jpg");
	return id;
}

void AgentManager::DeleteAgent(int id)
{
	agentClass[id]->Kill();
}

void AgentManager::GiveAgentMenuParas(int id, int statSpeedDeceaseMin, int statSpeedDeceaseMax, int agentLife)
{
	agentClass[id]->GiveMenuParas(statSpeedDeceaseMin, statSpeedDeceaseMax, agentLife);
}

void AgentManager::Update(float frameTime)
{
	//Update each agent
	for (int a = 0; a < agentClass.size(); a++) {
		agentClass[a]->Update(frameTime);
		if (agentClass[a]->GetHealth() <= 0) {
			agentClass[a]->Kill();
			uiClass->RemoveFromList(a);
		}
	}
}

void AgentManager::UpdateArrow(int i)
{
	if (i != -1) {
		int a = GetAgent(i);
		arrow->SetPosition(agentClass[a]->GetXPos(), 15.0f, agentClass[a]->GetZPos());
	}
}

void AgentManager::ShowArrow()
{
	arrow->SetY(15.0f);
}

void AgentManager::HideArrow()
{
	arrow->SetY(-10.0f);
}

vector<string> AgentManager::GetTaskList(int i)
{
	return agentClass[i]->GetTasks();
}

void AgentManager::ShowAgentPath(int i)
{
	agentClass[GetAgent(i)]->ShowPath(pathCube);
}

void AgentManager::HideAgentPath(int i)
{
	agentClass[GetAgent(i)]->HidePath(pathCube);
}

int AgentManager::GetHealth(int i)
{
	if (i != -1 && !agentClass.empty()) {
		return agentClass[GetAgent(i)]->GetHealth();
	}
	return -1;
}

int AgentManager::GetEnergy(int i)
{
	if (i != -1 && !agentClass.empty()) {
		return agentClass[GetAgent(i)]->GetEnergy();
	}
	return -1;
}

int AgentManager::GetThirst(int i)
{
	if (i != -1 && !agentClass.empty()) {
		return agentClass[GetAgent(i)]->GetThirst();
	}
	return -1;
}

int AgentManager::GetHunger(int i)
{
	if (i != -1 && !agentClass.empty()) {
		return agentClass[GetAgent(i)]->GetHunger();
	}
	return -1;
}

int AgentManager::GetWood(int i)
{
	if (i != -1 && !agentClass.empty()) {
		return agentClass[GetAgent(i)]->GetWood();
	}
	return -1;
}

int AgentManager::GetStone(int i)
{
	if (i != -1 && !agentClass.empty()) {
		return agentClass[GetAgent(i)]->GetStone();
	}
	return -1;
}