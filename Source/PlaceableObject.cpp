#include "PlaceableObject.h"

PlaceableObject::PlaceableObject(I3DEngine* tL, WorldGrid* wG)
{
	this->tL = tL;
	this->worldGrid = wG;

	for (int a = 0; a < amountOfObj; a++) {
		vector<SObjectData*> temp;
		objects.push_back(temp);
	}

	srand(time(NULL));
}

void PlaceableObject::UpgradeRoad(int x, int z)
{
	for (int a = 0; a < objects[Road].size(); a++) {
		if (objects[Road][a]->gridX == x && objects[Road][a]->gridZ == z) {
			objects[Road][a]->model->SetSkin("pathStone.jpg");
			objects[Road][a]->model->Scale(2.0f);
			worldGrid->ChangeTaken(5, x, z);
		}
	}
}

void PlaceableObject::CheckGirdSize(int x, int z, bool spawnObject)
{
	//return;
	if (x > worldGrid->GetXLenght() - 2) {
		worldGrid->AddRowTop();
		if (spawnObject)
			SpawnResource(worldGrid->GetXLenght() - 1, -1);
	}
	else if (x < 2) {
		worldGrid->AddRowBottom();
		for (int a = 0; a < objects.size(); a++) {
			for (int b = 0; b < objects[a].size(); b++) {
				objects[a][b]->gridZ += 1;
			}
		}
		if (spawnObject)
			SpawnResource(0, -1);
	}
	if (z > worldGrid->GetZLenght() - 2) {
		worldGrid->AddRowRight();
		if (spawnObject)
			SpawnResource(-1, worldGrid->GetZLenght() - 1);
	}
	else if (z < 2) {
		worldGrid->AddRowLeft();
		for (int a = 0; a < objects.size(); a++) {
			for (int b = 0; b < objects[a].size(); b++) {
				objects[a][b]->gridX += 1;
			}
		}
		if (spawnObject)
			SpawnResource(-1, 0);
	}
}

void PlaceableObject::GetAllObjectsLocation(vector<Objects>& obj, vector<int>& x, vector<int>& z)
{
	for (int _obj = 0; _obj < amountOfObj; _obj++) {
		for (int index = 0; index < objects[_obj].size(); index++) {
			obj.push_back(objects[_obj][index]->obj);
			x.push_back(objects[_obj][index]->gridX);
			z.push_back(objects[_obj][index]->gridZ);
		}
	}
}

void PlaceableObject::SpawnResoruceShortcut(Objects obj, string templat, int zRow, int lenght)
{
	bool loop = true;
	int loopNum = 0;
	int seed = rand() % 1000;
	while (loop) {
		srand(seed + loop);
		int r = rand() % lenght;
		if (CreateObject(Tree, "Tree", r, zRow + 0, 0.0f)) {
			loop = false;
		}
		loopNum++;
	}
}

void PlaceableObject::SpawnResource(int xRow, int zRow)
{
	int ranSpawnAmount = 0;
	if (xRow > -1) {
		ranSpawnAmount = rand() % worldGrid->GetZLenght() / 2;
	}
	else if (zRow > -1) {
		ranSpawnAmount = rand() % worldGrid->GetXLenght() / 2;
	}
	for (int a = 0; a < ranSpawnAmount; a++) {
		int percentNum = rand() % 100 + 1;
		if (percentNum <= 10) { //10% chance of spawning
			int ranNum = rand() % 2 + 0;
			if (xRow > -1) {
				if (ranNum == 0) {
					SpawnResoruceShortcut(Tree, "Tree", xRow, worldGrid->GetZLenght());
				}
				else if (ranNum == 1) {
					SpawnResoruceShortcut(Rock, "Rock", xRow, worldGrid->GetZLenght());
				}
				else if (ranNum == 2) {
					int fwefewf = 0;
				}
			}
			else if (zRow > -1) {
				if (ranNum == 0) {
					SpawnResoruceShortcut(Tree, "Tree", zRow, worldGrid->GetXLenght());
				}
				else if (ranNum == 1) {
					SpawnResoruceShortcut(Rock, "Rock", zRow, worldGrid->GetXLenght());
				}
				else if (ranNum == 2) {
					int fwefewf = 0;
				}
			}
		}
	}
}

void PlaceableObject::CreateTemplate(string templateName, string templateMesh,
	int foodstart, int waterStart, int rockStart, int treeStart)
{
	STemplate* temp = new STemplate(templateName, tL->LoadMesh(templateMesh));
	temp->foodStart = foodstart;
	temp->rockStart = rockStart;
	temp->treeStart = treeStart;
	temp->waterStart = waterStart;
	templates.push_back(temp);
}

int PlaceableObject::GetTemplateID(string templateName)
{
	for (int a = 0; a < templates.size(); a++) {
		if (templates[a]->name == templateName) {
			return a;
		}
	}
}

bool PlaceableObject::CreateObject(Objects obj, string templat, int gridX, int gridZ, int rot)
{
	if (worldGrid->GetTaken(gridX, gridZ) == -1) {
		//Create model
		float g = worldGrid->GetGridPosX(gridX, gridZ);
		float g1 = worldGrid->GetGridPosZ(gridX, gridZ);
		int templateID = GetTemplateID(templat);
		SObjectData* newObj = new SObjectData(
			obj,
			templates[templateID]->mesh->CreateModel(
				g,
				worldGrid->GetGridPosY(gridX, gridZ),
				g1
			),
			gridX, gridZ, 
			templates[templateID]->foodStart, 
			templates[templateID]->waterStart
		);
		//Changed to any model
		newObj->model->RotateLocalY(rot);

		//Changes to certain types of models
		switch (obj)
		{
		case Well:
			newObj->model->SetSkin("water.png");
			//newObj->model->Scale(0.5f);
			newObj->model->SetY(-4.5f);
			break;
		case KFC:
			//newObj->model->SetSkin("red.PNG");
			newObj->model->Scale(0.7f);
			newObj->model->SetY(2.0f);
			newObj->model->RotateLocalY(-45.0f);
			break;
		case House:
			newObj->model->Scale(6.0f);
			newObj->model->SetY(-3.7f);
			break;
		case Road:
			newObj->model->Scale(4.0f);
			newObj->model->SetSkin("dirt.jpg");
			newObj->model->SetY(0.2f);
			break;
		case CenterTower:
			newObj->model->Scale(5.0f);
			newObj->model->SetSkin("tiles1Green.png");
			break;
		case Rock:
			break;
		case Tree:
			newObj->model->Scale(3.0f);
			break;
		}
		objects[obj].push_back(newObj);
		worldGrid->ChangeTaken(obj, gridX, gridZ);
		return true;
	}
	return false;
}

void PlaceableObject::GetLocationOfObject(Objects obj, int index, int & x, int & z)
{
	x = objects[obj][index]->gridX;
	z = objects[obj][index]->gridZ;
}

PlaceableObject::SObjectData * PlaceableObject::GetObjectInformation(Objects obj, int index)
{
	return objects[obj][index];
}