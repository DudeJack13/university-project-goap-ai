#pragma once
#include <TL-Engine.h>	
#include <vector>
#include <time.h>
#ifndef WORLDGRID_H
#define WORLDGRID_H
#include "WorldGrid.h"
#endif
#ifndef GAMEOBJECTS_H
#define GAMEOBJECTS_H
#include "GameObjects.h"
#endif

using namespace tle;

class PlaceableObject
{
private:
	struct STemplate
	{
		STemplate(string name, IMesh* mesh) {
			this->name = name;
			this->mesh = mesh;
		}
		string name = "";
		IMesh* mesh;

		int foodStart = 0;
		int waterStart = 0;
		int rockStart = 0;
		int treeStart = 0;
	};
	vector<STemplate*> templates;

	struct SObjectData
	{
		SObjectData(Objects obj, IModel* model, int gridX, int gridZ, int currentFood, int currentWater) {
			this->obj = obj;
			this->model = model;
			this->gridX = gridX;
			this->gridZ = gridZ;
			this->currentFood = currentFood;
			this->currentWater = currentWater;
		}

		Objects obj;
		IModel* model;
		int gridX;
		int gridZ;
		int currentFood;
		int currentWater;
		int currentBuiltLevel = 0;
		int currentWood = 0;
		int currentStone = 0;
		int woodNeeded = 100;
		int stoneNeeded = 100;
		bool usable = false;
	};



	I3DEngine * tL;
	WorldGrid* worldGrid;

	vector<vector<SObjectData*>> objects;

	int GetTemplateID(string templateName);
	void SpawnResource(int xRow, int zRow);
	void SpawnResoruceShortcut(Objects obj, string templat, int zRow, int lenght);

public:
	PlaceableObject(I3DEngine*, WorldGrid*);
	~PlaceableObject() {}

	void CreateTemplate(string templateName, string templateMesh, 
		int foodstart, int waterStart, int rockStart, int treeStart);
	bool CreateObject(Objects obj, string templat, int gridX, int gridZ, int rot);
	void GetLocationOfObject(Objects obj, int index, int& x, int & z);
	int GetAmountOfObject(Objects obj) { return objects[obj].size(); }
	void UpgradeRoad(int x, int z);
	void CheckGirdSize(int x, int z, bool spawnObject);
	void GetAllObjectsLocation(vector<Objects>& obj, vector<int>& x, vector<int>& z);
	SObjectData* GetObjectInformation(Objects obj, int index);
};


