#include "Agent.h"

//Class methods
Agent::Agent(I3DEngine* tL, WorldGrid* worldGrid, PlaceableObject* placeableObjects, UIClass* uiClass, IMesh* mesh, int id, float x, float y, float z, int gridX, int gridZ)
{
	this->tL = tL;
	this->id = id;
	this->wG = worldGrid;
	this->uiClass = uiClass;
	this->placeableObjects = placeableObjects;
	model = mesh->CreateModel(x, y, z);
	agentMesh = mesh;
	model->Scale(scale);
	atXGrid = gridX;
	atZGrid = gridZ;
	cubeMesh = tL->LoadMesh("Cube.x");
	srand(time(NULL));
	int seed = rand() % 1000;
	srand(seed + id);
	SPEED = rand() % (10 - 25 + 1) + 10;
}

Agent::~Agent()
{
	goap->~GOAP();
	pathFinding->~PathFinding();

	agentMesh->RemoveModel(model);
	//tL->RemoveMesh(agentMesh);
	//tL->RemoveMesh(cubeMesh);
}

void Agent::LoadComponents(WorldGrid * wg)
{
	pathFinding = new PathFinding(wg);
	goap = new GOAP(wg, pathFinding, placeableObjects);
	goap->CreateTrees(vector<string>{"Sleep","Drink","Eat", "Place_House_Site", "Build_House", "Get_House_Items", "Attack"});
} 

void Agent::GiveMenuParas(int statSpeedDeceaseMin, int statSpeedDeceaseMax, int agentLife)
{
	this->hungryDeceaseSpeed = rand() % statSpeedDeceaseMax + statSpeedDeceaseMin;
	this->energyDeceaseSpeed = rand() % statSpeedDeceaseMax + statSpeedDeceaseMin;
	this->thirstDeceaseSpeed = rand() % statSpeedDeceaseMax + statSpeedDeceaseMin;
}

//Path methods
bool Agent::UpdateWalking(float frameTime)
{
	//model->Scale(1.0f);
	model->LookAt(shortPath[currentWaypoint].first, model->GetY(), shortPath[currentWaypoint].second);

	float disX = shortPath[currentWaypoint].first - model->GetX();
	float disZ = shortPath[currentWaypoint].second - model->GetZ();
	float dis = fabs(disX) + fabs(disZ);

	if (currentWaypoint != shortPath.size() - 1 && dis < 10.0f) {
		if (shortPath[currentWaypoint].first < shortPath[currentWaypoint + 1].first) {
			shortPath[currentWaypoint].first += (SPEED / 1.5f) * frameTime;
		}
		else if (shortPath[currentWaypoint].first > shortPath[currentWaypoint + 1].first) {
			shortPath[currentWaypoint].first -= (SPEED / 1.5f) * frameTime;
		}
		else if (shortPath[currentWaypoint].second < shortPath[currentWaypoint + 1].second) {
			shortPath[currentWaypoint].second += (SPEED / 1.5f) * frameTime;
		}
		else if (shortPath[currentWaypoint].second > shortPath[currentWaypoint + 1].second) {
			shortPath[currentWaypoint].second -= (SPEED / 1.5f) * frameTime;
		}
	}

	model->MoveLocalZ(SPEED * frameTime);
	model->Scale(scale);

	if (dis < 1.0f) {
		atXGrid = shortPathInt[currentWaypoint].first;
		atZGrid = shortPathInt[currentWaypoint].second;
		placeableObjects->CheckGirdSize(atXGrid, atZGrid, true);
		currentWaypoint++;
	}
	if (currentWaypoint == shortPath.size()) {
		currentWaypoint = 0;
		return true;
	}
	return false;
}

void Agent::DeletePath(IMesh* mesh)
{
	HidePath(mesh);
	path.clear();
	shortPath.clear();
	currentState = States::Idle;
}

bool Agent::CreatePath(int x, int z)
{
	vector<pair<int, int>> pathInt = pathFinding->GetPath(vector<int>{atXGrid, atZGrid}, vector<int>{x, z});
	if (pathInt.size() == 0) {
		uiClass->OutputToConsoleScreen("Agent" + to_string(id) + " path is blocked");
		return false;
	}
	else {
		shortPathInt = pathFinding->RemoveWaste(pathInt);
		path = pathFinding->ConvertPath(pathInt);
		shortPath = pathFinding->ConvertPath(shortPathInt);
		shortPathInt = pathFinding->ConvertPathInt(shortPathInt);
		return true;
	}
}

//Game methods
void Agent::HidePath(IMesh* mesh)
{
	for (int a = 0; a < cubePath.size(); a++) {
		mesh->RemoveModel(cubePath[a]);
	}
	cubePath.clear();
}

void Agent::GetWalkingLocationFromTask(string str, int&x, int& z)
{
	bool inBracket = false;
	bool first = true;
	string number1 = "";
	string number2 = "";
	for (char& c : tasks[0])
	{
		if (c == '(') {
			inBracket = true;
		}
		else if (inBracket) {
			if (c == ',') {
				first = false;
			}
			else if (c != ')'){
				if (first) {
					number1 += c;
				}
				else {
					number2 += c;
				}
			}
		}
	}
	x = stoi(number1);
	z = stoi(number2);
}

void Agent::RemovePathObjects()
{
	for (int a = 0; a < housePath.size(); a++) {
		cubeMesh->RemoveModel(housePath[a]);
	}
	housePath.clear();
}

void Agent::AddObjectPath(vector<pair<int, int>> path1)
{
	RemovePathObjects();
	vector<pair<float, float>> path2 = pathFinding->ConvertPath(path1);
	for (int g = 0; g < path2.size(); g++) {
		housePath.push_back(cubeMesh->CreateModel(path2[g].first, 0.0f, path2[g].second));
		housePath.back()->Scale(0.1f);
		housePath.back()->SetSkin("blue.png");
	}
}

void Agent::PlaceBuilding(Objects obj)
{
	//srand(time(NULL))
	bool loop = true;
	bool skip = false;
	int startX = 0;
	int startZ = 0;
	int loopNum = 0;
	int seed = rand() % 1000;
	placeableObjects->GetLocationOfObject(CenterTower, 0, startX, startZ);
	while (loop) {
		srand(seed + loopNum);
		if (placeableObjects->GetAmountOfObject(House) > 0) {
			if (5 % placeableObjects->GetAmountOfObject(House) == 0) {
				size++;
			}
		}
		int randX = rand() % ((startX + size) - (startX - size) + 1) + (startX - size); //Edit in loop to expand Biggest - smallest
		int randZ = rand() % ((startZ + size) - (startZ - size) + 1) + (startZ - size);
		if (wG->GetTaken(randX, randZ) == -1) {
			bool fine = true;
			for (int a = 0; a < 5; a++) {
				for (int b = 0; b < 5; b++) {
					if (randX == (X_DIR[a] + startX) && randZ == (Z_DIR[b] + startZ)) {
						fine = false;
						break;
					}
				}
			}
			if (fine) {
				vector<pair<int, int>> path1 = pathFinding->GetPath(vector<int>{randX, randZ}, vector<int>{startX, startZ});
				if (path1.size() > 0) {
					vector<Objects> _obj;
					vector<int> x;
					vector<int> z;
					int index = 0;
					placeableObjects->GetAllObjectsLocation(_obj, x, z);
					Objects lObj = _obj[0];
					for (int a = 0; a < _obj.size(); a++) {
						if (_obj[a] != lObj) {
							lObj = _obj[a];
							index = 0;
						}
						placeableObjects->GetLocationOfObject(_obj[a], index, x[a], z[a]);
						if ((randX >= startX && randX <= x[a] || randX <= startX && randX >= x[a]) &&
							(randZ >= startZ && randZ <= z[a] || randZ <= startZ && randZ >= z[a])) {
							vector<pair<int, int>> path2 = pathFinding->GetPath(vector<int>{x[a], z[a]}, vector<int>{startX, startZ});
							if (path2.size() > 0) {
								AddBuilding(House, randX, randZ, path1);
								//Test!

								return;
							}
							else {
								skip = true;
								break;
							}
							wG->ChangeTaken(0, randX, randZ);
						}
						index++;
					}
					if (!skip) {
						AddBuilding(House, randX, randZ, path1);
						return;
					}
					else {
						skip = false;
					}
				}
			}
		}
		loopNum++;
	}
}

void Agent::AddBuilding(Objects obj, int x, int z, vector<pair<int, int>> path1)
{
	for (int b = 0; b < path1.size(); b++) {
		wG->ChangeScore(wG->GetScore(path1[b].first, path1[b].second) - 10, path1[b].first, path1[b].second);
	}
	//AddObjectPath(path1);
	for (int a = 0; a < path1.size(); a++) {
		if (wG->GetTaken(path1[a].first, path1[a].second) == 0 && wG->GetScore(path1[a].first, path1[a].second) <= -30) {
			placeableObjects->CreateObject(Road, "Road", path1[a].first, path1[a].second, 0.0f);
		}
		else if (wG->GetTaken(path1[a].first, path1[a].second) == 4 && wG->GetScore(path1[a].first, path1[a].second) <= -100) {
			placeableObjects->UpgradeRoad(path1[a].first, path1[a].second);
		}
	}
	if (obj == House) {
		placeableObjects->CreateObject(obj, "House", x, z, 0.0f);
	}
	uiClass->OutputToConsoleScreen("Agent" + to_string(id) + " built a house at " + to_string(x) + ":" + to_string(z));
}

bool Agent::Collect(float frameTime)
{
	collectionTimer += 1.0f * frameTime;
	if (collectionTimer > 0.1f) {
		collectionTimer = 0.0f;

		if (currentState == Mining) {
			holdingStone += 10;
			//uiClass->OutputToConsoleScreen("Stone: " + to_string(holdingStone));
			if (holdingStone >= 100) {
				holdingStone = 100;
				return true;
			}
		}
		else if (currentState == Chopping) {
			holdingWood += 10;
			//uiClass->OutputToConsoleScreen("Wood: " + to_string(holdingWood));
			if (holdingWood >= 100) {
				holdingWood = 100;
				return true;
			}
		}
	}
	return false;
}

bool Agent::Build(float frameTime)
{
	buildTimer += 1.0f * frameTime;
	if (buildTimer > 0.1f) {
		buildTimer = 0;
		bool done1 = false;
		bool done2 = false;
		if (holdingStone >= 10) {
			placeableObjects->GetObjectInformation(House, houseId)->currentStone += 10;
			holdingStone -= 10;
			return false;
			//uiClass->OutputToConsoleScreen("House Stone: " + to_string(placeableObjects->GetObjectInformation(House, houseId)->currentStone));
		}
		else {
			done1 = true;
		}
		if (holdingWood >= 10) {
			placeableObjects->GetObjectInformation(House, houseId)->currentWood += 10;
			holdingWood -= 10;
			return false;
			//uiClass->OutputToConsoleScreen("House Wood: " + to_string(placeableObjects->GetObjectInformation(House, houseId)->currentWood));
		}
		else {
			done2 = true;
		}
		if (done1 && done2) {
			return true;
		}
	}
	return false;
}

Agent::States Agent::UpdateState()
{
	int added = hungryLevel + energyLevel + thirstLevel;
	float hLf = ((float)hungryLevel / (float)added) * 100.0f;
	float eLf = ((float)energyLevel / (float)added) * 100.0f;
	float tLf = ((float)thirstLevel / (float)added) * 100.0f;

	if (tasks.size() == currentTask) {
		currentTask = 0;
		tasks.clear();
	}
	if (hLf > 30.0f && eLf > 15.0f && tLf > 20.0f) {
		if (lifeState == Alive) {
			if (houseId == -1) {
				tasks = goap->GetTasks("Place_House_Site", atXGrid, atZGrid);
				return States::Place_House_Site;
			}
			if (placeableObjects->GetObjectInformation(House, houseId)->currentStone <
				placeableObjects->GetObjectInformation(House, houseId)->stoneNeeded &&
				placeableObjects->GetObjectInformation(House, houseId)->currentWood <
				placeableObjects->GetObjectInformation(House, houseId)->woodNeeded) {
				if (holdingWood == 0 || holdingStone == 0) {
					tasks = goap->GetTasks("Get_House_Items", atXGrid, atZGrid);
				}
				else {
					tasks = goap->GetTasks("Build_House", atXGrid, atZGrid);
				}
				return States::NextTask;
			}
		}
		//do work task
		if (!working) {
			if (lifeState == Zombie) {
				//tasks = goap->GetTasks("Attack", atXGrid, atZGrid);
				//return NextTask;
			}
			int x = rand() % ((wG->GetXLenght() - 3) - (3) + 1) + (3);
			int z = rand() % ((wG->GetZLenght() - 3) - (3) + 1) + (3);
			string work = "Move(" + to_string(x) + "," + to_string(z) + ")";
			tasks.push_back(work);
			working = true;
			//currentTask = 0;
			return NextTask;
		}
	}
	working = false;
	if (lifeState == Alive) {
		if (tLf <= hLf && tLf <= eLf) {
			tasks = goap->GetTasks("Drink", atXGrid, atZGrid);
			return States::NextTask;
		}
	}
	if (hLf <= tLf && hLf <= eLf) {
		tasks = goap->GetTasks("Eat", atXGrid, atZGrid);
		return States::NextTask;
	}
	if (eLf <= tLf && eLf <= hLf && houseId != -1) {
		tasks = goap->GetTasks("Sleep", atXGrid, atZGrid);
		return States::NextTask;
	}
	return States::Idle;
}

void Agent::Update(float frameTime)
{
	if (lifeState == Zombie) {
		switch (currentState)
		{
		case NextTask:
			if (currentTask < tasks.size()) {
				if (tasks[currentTask].find("Move") != std::string::npos) {
					int x;
					int z;
					GetWalkingLocationFromTask(tasks[currentTask], x, z);
					if (CreatePath(x, z)) { //Path finding error if at same spot
						currentState = States::Walking;
					}
					else {
						currentState = States::Idle;
					}
				}
				//else if (tasks[currentTask].find("Attack") != std::string::npos) {
				//	currentState = States::Eating;
				//}
			}
			else {
				currentState = States::Idle;
			}
		case Walking:
			if (UpdateWalking(frameTime)) {
				currentTask++;
				currentState = States::NextTask;
			}
			break;
		case Idle:
			currentState = UpdateState();
			break;
		default:
			break;
		}
	}
	else if (lifeState == Alive) {
		needTimer += 1.0f * frameTime;

		float well1 = placeableObjects->GetObjectInformation(Well, 1)->model->GetX();
		float well2 = placeableObjects->GetObjectInformation(Well, 1)->model->GetZ();

		float temp1 = placeableObjects->GetObjectInformation(Rock, 0)->model->GetX();
		float temp2 = placeableObjects->GetObjectInformation(Rock, 0)->model->GetZ();

		if (needTimer > 1.0f) {
			hungryLevel -= (hungryDeceaseSpeed / 10);
			energyLevel -= (energyDeceaseSpeed / 10);
			thirstLevel -= (thirstDeceaseSpeed / 10);
			needTimer = 0.0f;
		}

		if (hungryLevel < 0) { hungryLevel = 0; }
		if (energyLevel < 0) { energyLevel = 0; }
		if (thirstLevel < 0) { thirstLevel = 0; }

		if (hungryLevel == 0 || thirstLevel == 0) {
			health -= 0.05 * frameTime;
		}

		switch (currentState)
		{
		case NextTask:
			if (currentTask < tasks.size()) {
				if (tasks[currentTask].find("Move") != std::string::npos) {
					int x;
					int z;
					GetWalkingLocationFromTask(tasks[currentTask], x, z);
					if (CreatePath(x, z)) { //Path finding error if at same spot
						currentState = States::Walking;
					}
					else {
						currentState = States::Idle;
					}
				}
				else if (tasks[currentTask].find("Drink") != std::string::npos) {
					currentState = States::Drinking;
				}
				else if (tasks[currentTask].find("Eat") != std::string::npos) {
					currentState = States::Eating;
				}
				else if (tasks[currentTask].find("Mine") != std::string::npos) {
					currentState = States::Mining;
				}
				else if (tasks[currentTask].find("Chop") != std::string::npos) {
					currentState = States::Chopping;
				}
				else if (tasks[currentTask].find("Construct") != std::string::npos) {
					currentState = States::Build_House;
				}
				else if (tasks[currentTask].find("Sleep") != std::string::npos) {
					currentState = States::Sleep;
				}
			}
			else {
				currentState = States::Idle;
			}
			break;
		case Idle:
			currentState = UpdateState();
			break;
		case Walking:
			if (UpdateWalking(frameTime)) {
				currentTask++;
				currentState = States::NextTask;
			}
			break;
		case Drinking:
			actionTimer += 0.1f * frameTime;
			if (actionTimer > 1.0f) {
				actionTimer = 0.0f;
				thirstLevel += 5;
				if (thirstLevel >= 100) {
					thirstLevel = 100;
					currentTask++;
					currentState = States::Idle;
				}
			}
			break;
		case Eating:
			actionTimer += 0.1f * frameTime;
			if (actionTimer > 1.0f) {
				actionTimer = 0.0f;
				hungryLevel += 5;
				if (hungryLevel >= 100) {
					hungryLevel = 100;
					currentTask++;
					currentState = States::Idle;
				}
			}
			break;
		case Sleep:
			actionTimer += 0.1f * frameTime;
			if (actionTimer > 1.0f) {
				actionTimer = 0.0f;
				energyLevel += 5;
				if (energyLevel >= 100) {
					energyLevel = 100;
					currentTask++;
					currentState = States::Idle;
				}
			}
			break;
		case Place_House_Site:
			PlaceBuilding(House);
			needToBuildHouse = 70.0f;
			houseId = placeableObjects->GetAmountOfObject(House) - 1;
			goap->UpdateHouseID(houseId);
			currentState = States::Idle;
			break;
		case Mining:
			if (Collect(frameTime)) {
				currentTask++;
				currentState = Idle;
			}
			break;
		case Chopping:
			if (Collect(frameTime)) {
				currentTask++;
				currentState = Idle;
			}
			break;
		case Build_House:
			if (Build(frameTime)) {
				currentTask++;
				currentState = Idle;
			}
			break;
		case Work:
			currentState = UpdateState();
			break;
		default:
			currentState = States::Idle;
			break;
		}
		goap->GiveAgentInventory(holdingWood, holdingStone, currentState);
	}
}

void Agent::ShowPath(IMesh* mesh)
{
	for (int a = currentWaypoint; a < path.size(); a++) {
		cubePath.push_back(mesh->CreateModel(path[a].first, 0.0f, path[a].second));
		cubePath.back()->Scale(0.1f);
		cubePath.back()->SetSkin("blue.png");
	}
}

vector<string> Agent::GetTasks()
{
	vector <string> returnTask;
	for (int a = currentTask; a < tasks.size(); a++) {
		returnTask.push_back(tasks[a]);
	}
	return returnTask;
}

string Agent::GetStatus()
{
	//return currentState;
	if (currentState == States::Idle) { return "Idle"; }
	else if (currentState == States::Walking) { return "Walking"; }
	else if (currentState == States::Drinking) { return "Drinking"; }
	else if (currentState == States::Eating) { return "Eating"; }
	else if (currentState == States::Mining) { return "Mining"; }
	else if (currentState == States::Chopping) { return "Chopping"; }
	else if (currentState == States::Build_House) { return "Building House"; }
	else if (currentState == States::Work) { return "Work"; }
	else if (currentState == States::Sleep) { return "Sleep"; }
	return "Blank";
}