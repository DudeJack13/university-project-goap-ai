#pragma once

#include <TL-Engine.h>
#include "UIElements.h"

using namespace tle;

class MainMenu
{
private:
	I3DEngine* tL;
	IFont* titleFont;
	IFont* normalFont;
	IFont* debugFont;

	OptionBox* amountOfAgentsBox;
	OptionBox* agentStatMin;
	OptionBox* agentStatMax;
	OptionBox* agentMaxLife;
	Button* startButton;

	ISprite* box1;

	const int yPos = 30;
	const int xPos = 50;

	OptionBox* currentTextBoxSelected = nullptr;

	string GetInput();

public:
	MainMenu(I3DEngine* tL);
	~MainMenu() {}

	void Kill();

	bool Update();
	int GetAgentAmount();// { return amountOfAgentsBox->GetValue();  }
	int GetAgentStatMin();// { return agentStatMin->GetValue(); }
	int GetAgentStatMax();// { return agentStatMax->GetValue(); }
	int GetAgentMaxLife();// { return agentMaxLife->GetValue(); }
};

