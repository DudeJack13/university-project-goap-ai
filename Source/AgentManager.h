#pragma once
#include <TL-Engine.h>	
#include <vector>
#include "Agent.h"
#ifndef WORLDGRID_H
#define WORLDGRID_H
#include "WorldGrid.h"
#endif
#ifndef PLACEABLEOJBJECTS_H
#define PLACEABLEOJBJECTS_H
#include "PlaceableObject.h"
#endif
#ifndef UICLASS_H
#define UICLASS_H
#include "UIClass.h"
#endif 

using namespace tle;

class AgentManager
{
private:
	//Meshes
	IMesh* agentMesh1;
	IMesh* arrowMesh;
	IMesh* pathCube;

	//Models
	IModel* arrow;

	//Classes
	vector<Agent*> agentClass;
	WorldGrid* worldGrid;
	PlaceableObject* placeableObjects;
	UIClass* uiClass;

	//Egine
	I3DEngine* tL;

	//Varaibles
	const float AGENT_HEIGHT = 0.8f;
	int agentIdCounter = 0;

	//Functions
	int GetAgent(int);

public:
	AgentManager(I3DEngine*, WorldGrid*, PlaceableObject*, UIClass*);
	~AgentManager() {}

	void Update(float frameTime);

	int SpawnAgent(int gridX, int gridZ);
	int SpawnZombie(int gridX, int gridZ);
	void GiveAgentMenuParas(int id, 
		int statSpeedDeceaseMin,
		int statSpeedDeceaseMax,
		int agentLife
	);
	void DeleteAgent(int id);
	
	void UpdateArrow(int);
	void HideArrow();
	void ShowArrow();
	void ShowAgentPath(int);
	void HideAgentPath(int);

	vector<string> GetTaskList(int id);
	int GetAmountOfAgents() { return agentClass.size(); }
	
	int GetHealth(int);
	int GetEnergy(int);
	int GetThirst(int);
	int GetHunger(int);
	int GetWood(int);
	int GetStone(int);
	string GetStatus(int i) { return agentClass[i]->GetStatus(); }
};

