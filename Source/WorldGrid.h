#include <TL-Engine.h>	
#include <vector>
#include <time.h>
using namespace tle;

class WorldGrid
{
public:
	struct GridPos
	{
		float x = 0.0f;
		float y = -0.35f;
		float z = 0.0f;
		int taken = -1;
		int score = 0;
	};
	WorldGrid();
	~WorldGrid() {}
	int GetXLenght();
	int GetZLenght();
	float GetGridPosX(int x, int z);
	float GetGridPosY(int x, int z);
	float GetGridPosZ(int x, int z);
	void AddRowTop();
	void AddRowBottom();
	void AddRowLeft();
	void AddRowRight();
	void ChangeTaken(int value, int x, int z);
	int GetTaken(int x, int z);
	void ChangeScore(int value, int x, int z);
	int GetScore(int x, int z);
	vector<vector<GridPos>> GetGrid();
private:
	//Z foward, X side
	vector<vector<GridPos>> mainGrid;
	const int gridSize = 10;
	int startXLenght = 25;
	int startZLenght = 25;
	int currentGridSizeDown = 0;
	int currentGridSizeUp = startXLenght - 1;
	int currentGridRight = startZLenght - 1;
	int currentGridLeft = 0;
	int sideAmount = startXLenght;
	int upAmount = startZLenght;

	void CreateGrid();
	vector<GridPos> CreateRow(int);
	vector<GridPos> CreateColumn(int);
};

