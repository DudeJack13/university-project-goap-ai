#include "MainMenu.h"



MainMenu::MainMenu(I3DEngine* tL)
{
	this->tL = tL;
	titleFont = tL->LoadFont("Font1.bmp", 44);
	normalFont = tL->LoadFont("Font1.bmp", 30);
	debugFont = tL->LoadFont("Font1.bmp", 15);
	//ISprite
	amountOfAgentsBox = new OptionBox(tL->CreateSprite("MainMenu-TextBox.png", xPos + 360, yPos + 100, 0), "Spawn Agent                       : ", xPos, yPos + 100, 109, 29);
	agentStatMin = new OptionBox(tL->CreateSprite("MainMenu-TextBox.png", xPos + 360, yPos + 130, 0), "Random Agent Stats Range : ", xPos, yPos + 130, 109, 29);
	agentStatMax = new OptionBox(tL->CreateSprite("MainMenu-TextBox.png", xPos + 500, yPos + 130, 0), " - ", xPos + 472, yPos + 128, 109, 29);
	agentMaxLife = new OptionBox(tL->CreateSprite("MainMenu-TextBox.png", xPos + 360, yPos + 160, 0), "Agent Max Life                     : ", xPos, yPos + 160, 109, 29);
	startButton = new Button(tL->CreateSprite("MainMenu-Start_Light.png", xPos + 627, yPos + 210, 0), "MainMenu-Start_Light.png", "MainMenu-Start_Dark.png", 182, 66);
	box1 = tL->CreateSprite("MainMenu-SubBox.png", xPos - 10, yPos - 5, 1);
}

void MainMenu::Kill()
{
	tL->RemoveFont(titleFont);
	tL->RemoveFont(normalFont);
	tL->RemoveFont(debugFont);
	tL->RemoveSprite(box1);
	tL->RemoveSprite(amountOfAgentsBox->sprite);
	tL->RemoveSprite(agentStatMin->sprite);
	tL->RemoveSprite(agentStatMax->sprite);
	tL->RemoveSprite(agentMaxLife->sprite);
	tL->RemoveSprite(startButton->sprite);

	delete(amountOfAgentsBox);
	delete(agentMaxLife);
	delete(agentStatMax);
	delete(agentStatMin);
	//if (currentTextBoxSelected != nullptr) {
	//	delete(currentTextBoxSelected);
	//}
	delete(startButton);
}

bool MainMenu::Update()
{
	titleFont->Draw("Project Simulator: \nGOAP AI", xPos, yPos);

	normalFont->Draw(amountOfAgentsBox->label, amountOfAgentsBox->x, amountOfAgentsBox->y);
	normalFont->Draw(agentStatMin->label, agentStatMin->x, agentStatMin->y);
	normalFont->Draw(agentStatMax->label, agentStatMax->x, agentStatMax->y);
	normalFont->Draw(agentMaxLife->label, agentMaxLife->x, agentMaxLife->y);

	normalFont->Draw(amountOfAgentsBox->inputText, amountOfAgentsBox->sprite->GetX() + 5, amountOfAgentsBox->sprite->GetY());
	normalFont->Draw(agentStatMin->inputText, agentStatMin->sprite->GetX() + 5, agentStatMin->sprite->GetY());
	normalFont->Draw(agentStatMax->inputText, agentStatMax->sprite->GetX() + 5, agentStatMax->sprite->GetY());
	normalFont->Draw(agentMaxLife->inputText, agentMaxLife->sprite->GetX() + 5, agentMaxLife->sprite->GetY());

	int mouseX = tL->GetMouseX();
	int mouseY = tL->GetMouseY();

	startButton->HoverUpdate(tL, mouseX, mouseY);

	if (tL->KeyHit(Mouse_LButton)) {
		if (amountOfAgentsBox->Selected(mouseX, mouseY)) {
			currentTextBoxSelected = amountOfAgentsBox;
		}
		else if (agentStatMin->Selected(mouseX, mouseY)) {
			currentTextBoxSelected = agentStatMin;
		}
		else if (agentStatMax->Selected(mouseX, mouseY)) {
			currentTextBoxSelected = agentStatMax;
		}
		else if (agentMaxLife->Selected(mouseX, mouseY)) {
			currentTextBoxSelected = agentMaxLife;
		}
		else if (startButton->IsHovered()) {
			return true;
		}
		else {
			currentTextBoxSelected = nullptr;
		}
	}

	string input = GetInput();
	if (input != "" && currentTextBoxSelected != nullptr) {
		if (input == "return") {
			if (currentTextBoxSelected->inputText.size() > 0) {
				string temp = "";
				for (int a = 0; a < currentTextBoxSelected->inputText.size() - 1; a++) {
					temp += currentTextBoxSelected->inputText[a];
				}
				currentTextBoxSelected->inputText = temp;
			}
		}
		else {
			if (currentTextBoxSelected->inputText.size() < 6) {
				currentTextBoxSelected->inputText += input;
			}
		}
	}


	return false;
}

string MainMenu::GetInput()
{
	if (tL->KeyHit(Key_0)) {
		return "0";
	}
	else if (tL->KeyHit(Key_1)) {
		return "1";
	}
	else if (tL->KeyHit(Key_2)) {
		return "2";
	}
	else if (tL->KeyHit(Key_3)) {
		return "3";
	}
	else if (tL->KeyHit(Key_4)) {
		return "4";
	}
	else if (tL->KeyHit(Key_5)) {
		return "5";
	}
	else if (tL->KeyHit(Key_6)) {
		return "6";
	}
	else if (tL->KeyHit(Key_7)) {
		return "7";
	}
	else if (tL->KeyHit(Key_8)) {
		return "8";
	}
	else if (tL->KeyHit(Key_9)) {
		return "9";
	}
	else if (tL->KeyHit(Key_Back)) {
		return "return";
	}
	return "";
}

int MainMenu::GetAgentAmount() {
	if (amountOfAgentsBox->GetValue() > 0) {
		return amountOfAgentsBox->GetValue();
	}
	return 5 ;
}

int MainMenu::GetAgentStatMin() {
	if (agentStatMin->GetValue() > 0) {
		return agentStatMin->GetValue();
	}
	return 2;
}

int MainMenu::GetAgentStatMax() {
	if (agentStatMax->GetValue() > 0) {
		return agentStatMax->GetValue();
	}
	return 5;
}

int MainMenu::GetAgentMaxLife() {
	if (agentMaxLife->GetValue() > 0) {
		return agentMaxLife->GetValue();
	}
	return 100;
}