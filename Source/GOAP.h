#pragma once
#include <vector>
#include "ParseGoalDocs.h"
#ifndef PATHFINDING_H
#define PATHFINDING_H
#include "PathFinding.h"
#endif
#ifndef WORLDGRID_H
#define WORLDGRID_H
#include "WorldGrid.h"
#endif
#ifndef PLACEABLEOJBJECTS_H
#define PLACEABLEOJBJECTS_H
#include "PlaceableObject.h"
#endif
#ifndef GAMEOBJECTS_H
#define GAMEOBJECTS_H
#include "GameObjects.h"
#endif

using namespace std;

class GOAP
{
private:
	ParseGoalDocs* parseGoalDocs;

	PathFinding* pathFinding;
	WorldGrid* worldGrid;
	PlaceableObject* placeableObjects;

	struct Tasks {
		Tasks(
			string goal,
			vector<string> needs,
			int score,
			string outcome) {
			this->goal = goal;
			this->outcome = outcome;
			this->needs = needs;
			this->score = score;
		}
		string goal;
		vector<string> needs;
		int score;
		string outcome;
		int currentScore = 0;
		vector<Tasks*> childTask;
		Tasks* parent;
	};
	vector<Tasks*> tasks;
	vector<Tasks*> tree;

	int agentHouseID = -1;
	int agentWood = 0;
	int agentStone = 0;
	int agentState = 0;

	struct CompareTasks
	{
		inline bool operator() (Tasks* lhs, Tasks* rhs) {
			return lhs->currentScore < rhs->currentScore;
		}
	};

	vector<pair<int, int>> GetClosesItem(string item, int gridXStart, int gridZStart, string& returnGoal);
	bool ValidatedIfReal(string object);

	bool FindWord(string line, string s) { return line.find(s) != std::string::npos; }
	string RemoveTag(string object);
	int GetMoveScore(string locationName, int gridXStart, int gridZStart, string& returnGoal);
	bool CheckAgentInventory(string task);
	

public:
	GOAP(WorldGrid* worldGrid, PathFinding* pathFinding, PlaceableObject* placeableObjects);
	~GOAP();

	void CreateTrees(vector<string> tasks);
	vector<string> GetTasks(string goal, int gridXStart, int gridZStart);
	void CheckScoresUpdate();

	void UpdateHouseID(int id) { agentHouseID = id; }
	void GiveAgentInventory(int wood, int stone, int state);
};

