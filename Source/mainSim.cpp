
#include "mainSim.h"

MainSim::MainSim(tle::I3DEngine* tL)
{
	//Set Up
	this->tL = tL;
	//Load main menu stuff
	//Load UI for loading screen
	mainMenu = new MainMenu(tL);
}

void MainSim::LoadObjects()
{
	srand(time(NULL));
	//Create classes
	worldGrid = new WorldGrid();
	uiClass->OutputToConsoleScreen("LOADED: WorldGridClass");
	uiClass->OutputToConsoleScreen("LOADED: HighLigherClass");
	placeableObjects = new PlaceableObject(this->tL, worldGrid);
	uiClass->OutputToConsoleScreen("LOADED: PlaceableObjectsClass");
	agentManager = new AgentManager(this->tL, worldGrid, placeableObjects, uiClass);
	uiClass->OutputToConsoleScreen("LOADED: AgentManagerClass");
	//Create models
	skyboxMesh = tL->LoadMesh("Skybox.x");
	uiClass->OutputToConsoleScreen("LOADED: Skybox.x");
	floorMesh = tL->LoadMesh("Floor.x");
	uiClass->OutputToConsoleScreen("LOADED: Floor.x");
	cubeMesh = tL->LoadMesh("Cube.x");
	uiClass->OutputToConsoleScreen("LOADED: Cube.x");
}

void MainSim::LoadGame()
{
	skybox = skyboxMesh->CreateModel(0.0f, -550.0f, 0.0f);
	uiClass->OutputToConsoleScreen("CREATED: Skybox");
	floor = floorMesh->CreateModel(0.0f, 0.0f, 0.0f);
	floor->SetSkin("grass.png");
	uiClass->OutputToConsoleScreen("CREATED: Floor");
	camera = tL->CreateCamera(tle::kManual, 0.0f, 100.0f, -100.0f);
	camera->RotateLocalX(45.0f);
	cameraBaseModel = cubeMesh->CreateModel(125.0f, -10.0f, 125.0f);
	camera->AttachToParent(cameraBaseModel);
	cameraBaseModel->RotateLocalY(cameraRotation);
	uiClass->OutputToConsoleScreen("CREATED: Camera");
	//Creating objects in game
	
	uiClass->OutputToConsoleScreen("Loading Meshes");
	placeableObjects->CreateTemplate("Well", "Cube.x", 0, 100, 0, 0);
	placeableObjects->CreateTemplate("KFC", "cow.x", 100, 0, 0, 0);
	placeableObjects->CreateTemplate("House", "factory.x", 100, 100, 0, 0);
	placeableObjects->CreateTemplate("CenterTower", "guardTower1.x", 0, 0, 0, 0);
	placeableObjects->CreateTemplate("Road", "pathblock.x", 0, 0, 0, 0);
	placeableObjects->CreateTemplate("Tree", "Tree1.x", 0, 0, 0, 100);
	placeableObjects->CreateTemplate("Rock", "rock.x", 0, 0, 100, 0);

	uiClass->OutputToConsoleScreen("Loading Models");

	placeableObjects->CreateObject(Tree, "Tree", 11, 14, 0.0f);
	placeableObjects->CreateObject(Rock, "Rock", 10, 11, 0.0f);

	placeableObjects->CreateObject(CenterTower, "CenterTower", 12, 12, 0.0f);
	placeableObjects->CreateObject(Road, "Road", 11, 11, 0.0f);
	placeableObjects->CreateObject(Road, "Road", 11, 12, 0.0f);
	placeableObjects->CreateObject(Road, "Road", 11, 13, 0.0f);
	placeableObjects->CreateObject(Road, "Road", 12, 11, 0.0f);
	placeableObjects->CreateObject(Road, "Road", 12, 13, 0.0f);
	placeableObjects->CreateObject(Road, "Road", 13, 11, 0.0f);
	placeableObjects->CreateObject(Road, "Road", 13, 12, 0.0f);
	placeableObjects->CreateObject(Road, "Road", 13, 13, 0.0f);

	placeableObjects->CreateObject(Well, "Well", 10, 12, 0.0f);
	placeableObjects->CreateObject(KFC, "KFC", 10, 13, 0.0f);
	//placeableObjects->CreateObject(PlaceableObject::Objects::Tree, "Tree", 2, 2, 0.0f);
	//placeableObjects->CreateObject(PlaceableObject::Objects::Rock, "Rock", 0, 0, 0.0f);

	for (int x = 0; x < worldGrid->GetXLenght(); x++) {
		for (int z = 0; z < worldGrid->GetZLenght(); z++) {
			if (rand() % 100 < density) {
				int r = rand() % 4;
				int rot = rand() % 360;
				if (r == 0) {
					placeableObjects->CreateObject(Tree, "Tree", x, z, (float)rot);
				}
				else if (r == 1) {
					placeableObjects->CreateObject(Rock, "Rock", x, z, (float)rot);
				}
				r = rand() % 4;
				if (r == 2) {
					placeableObjects->CreateObject(Well, "Well", x, z, 0.0f);
				}
				else if (r == 3) {
					placeableObjects->CreateObject(KFC, "KFC", x, z, 0.0f);
				}
			}
		}
	}

	//createGridHightlight();

	for (int a = 0; a < amountOfAgents; a++) {
		int id = agentManager->SpawnAgent(12, 11);
		agentManager->GiveAgentMenuParas(id, agentStatMin, agentStatMax, agentMaxLife);
		uiClass->AddToList(id, "Agent" + to_string(id));
	}
	uiClass->OutputToConsoleScreen("Loaded " + to_string(amountOfAgents) + " agents");
	uiClass->OutputToConsoleScreen("Models loaded");
}

bool MainSim::Update(float frameTime)
{
	if (sim_State != Sim_States::Menu) {
		uiClass->DrawDebug(frameTime);
		uiClass->DrawOutputUI(frameTime);
	}
	//Main
	if (tL->KeyHit(tle::Key_Escape)) {
		return true;
	}
	else if (sim_State == Sim_States::Menu) {
		if (mainMenu->Update()) {

			amountOfAgents = mainMenu->GetAgentAmount();
			agentStatMax = mainMenu->GetAgentStatMax();
			agentStatMin = mainMenu->GetAgentStatMin();
			agentMaxLife = mainMenu->GetAgentMaxLife();

			mainMenu->Kill();
			mainMenu->~MainMenu();
			delete(mainMenu);

			uiClass = new UIClass(this->tL);
			uiClass->LoadLoadingUI();
			LoadObjects();
			uiClass->LoadGameUI();
			LoadGame();
			sim_State = InSimulation;
		}
	}
	else if (sim_State == Sim_States::InSimulation) {
		//Debug controls
		//DebugControls();
		//UI class handling
		uiClass->UpdateMouse();
		if (uiClass->GetInUIState()) {
			uiClass->ScrollInList(tL->GetMouseWheelMovement());
		}
		//UI buttons 
		if (tL->KeyHit(Mouse_LButton)) {
			uiClass->UIButtons(showArrow, showPath);
			//Arrow buttons
			if (!showArrow) {
				agentManager->HideArrow();
			}
			else if (showArrow) {
				agentManager->ShowArrow();
			}
			//Show path
			if (!showPath) {
				if (currentlyShownAgentPath != -1) {
					agentManager->HideAgentPath(currentlyShownAgentPath);
				}
			}
			else if (showPath) {
				int current = uiClass->GetCurrentSelected();
				agentManager->ShowAgentPath(current);
				currentlyShownAgentPath = current;
			}
		}
		//Update Arrow
		if (showArrow) {
			agentManager->UpdateArrow(uiClass->GetCurrentSelected());
		}
		//UI print everything 
		uiClass->PrintText();
		if (agentManager->GetAmountOfAgents() > 0) {
			uiClass->PrintAIInformation(
				agentManager->GetStatus(uiClass->GetCurrentSelected()),
				agentManager->GetHunger(uiClass->GetCurrentSelected()),
				agentManager->GetEnergy(uiClass->GetCurrentSelected()),
				agentManager->GetThirst(uiClass->GetCurrentSelected()),
				agentManager->GetHealth(uiClass->GetCurrentSelected()),
				agentManager->GetWood(uiClass->GetCurrentSelected()),
				agentManager->GetStone(uiClass->GetCurrentSelected())
				);
			uiClass->UpdateTaskList(agentManager->GetTaskList(uiClass->GetCurrentSelected()));
		}

		//Update camera
		UpdateCamera(frameTime);
		//Agent handling 
		
		agentManager->Update(frameTime);

	}
	return false;
}

void MainSim::DebugControls()
{
	//Debug
	if (tL->KeyHit(Key_1)) {
		int id = agentManager->SpawnAgent(4, 3);
		uiClass->AddToList(id, "Agent" + to_string(id));
		uiClass->OutputToConsoleScreen("Agent Spawned");
	}

	if (tL->KeyHit(Key_3)) {
		int id = agentManager->SpawnZombie(3, 3);
		agentManager->GiveAgentMenuParas(id, agentStatMin, agentStatMax, agentMaxLife);
		uiClass->AddToList(id, "Zom" + to_string(id));
	}

	if (tL->KeyHit(Key_2)) {
		agentManager->DeleteAgent(2);
		uiClass->RemoveFromList(2);
	}

}

void MainSim::UpdateCamera(float frameTime)
{
	//Camera movement
	if (tL->KeyHeld(tle::Key_W)) {
		cameraBaseModel->MoveLocalZ(cameraSpeed * frameTime);
	}
	else if (tL->KeyHeld(tle::Key_S)) {
		cameraBaseModel->MoveLocalZ(-cameraSpeed * frameTime);
	}
	if (tL->KeyHeld(tle::Key_A)) {
		cameraBaseModel->MoveLocalX(-cameraSpeed * frameTime);
	}
	else if (tL->KeyHeld(tle::Key_D)) {
		cameraBaseModel->MoveLocalX(cameraSpeed * frameTime);
	}

	//Camera rotation 
	if (!cameraRotateLeft && !cameraRotateRight) {
		if (tL->KeyHit(tle::Key_Q)) {
			cameraRotateLeft = true;
			startRot = cameraRotation;
		}
		else if (tL->KeyHit(tle::Key_E)) {
			cameraRotateRight = true;
			startRot = cameraRotation;
		}
	}
	else {
		if (cameraRotateLeft) {
			float dif = CAMERA_ROTATION_SPEED * frameTime;
			cameraRotation += dif;
			cameraBaseModel->RotateLocalY(dif);
			if (cameraRotation > startRot + 90.0f) {
				cameraRotateLeft = false;
			}
		}
		else if (cameraRotateRight) {
			float dif = CAMERA_ROTATION_SPEED * frameTime;
			cameraRotation -= dif;
			cameraBaseModel->RotateLocalY(-dif);
			if (cameraRotation < startRot - 90.0f) {
				cameraRotateRight = false;
			}
		}
	}
	
	if (tL->GetMouseWheel() > zoomScroll || tL->KeyHit(tle::Key_Up)) {
		zoomScroll = tL->GetMouseWheel();
		if (zoomLevel >= 3) {
			camera->SetLocalZ(camera->GetLocalZ() + CAMERA_ZOOM_SPEED);
			camera->SetLocalY(camera->GetLocalY() - CAMERA_ZOOM_SPEED);
			zoomLevel--;
		}
	}
	else if (tL->GetMouseWheel() < zoomScroll || tL->KeyHit(tle::Key_Down)) {
		zoomScroll = tL->GetMouseWheel();
		if (zoomLevel <= 15) {
			camera->SetLocalZ(camera->GetLocalZ() - CAMERA_ZOOM_SPEED);
			camera->SetLocalY(camera->GetLocalY() + CAMERA_ZOOM_SPEED);
			zoomLevel++;
		}
	}
}
