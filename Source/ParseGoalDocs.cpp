#include "ParseGoalDocs.h"

int ParseGoalDocs::GetFileModTime(string file)
{
	ifstream scoreFile(file);
	struct stat st;
	const char* ch = file.c_str();
	stat(ch, &st);
	return st.st_mtime;
}

bool ParseGoalDocs::IfCurrentScoreFile(string inScoresFile)
{
	return (GetFileModTime(inScoresFile) > scoreFileModTime);
}

string ParseGoalDocs::RemoveTag(string line, string tag)
{
	int tagLenght = tag.length();
	string returnString = "";
	for (char& c : line) {
		if (tagLenght > 0) {
			tagLenght--;
		}
		else {
			returnString += c;
		}
	}
	return returnString;
}

void ParseGoalDocs::Clear()
{
	goal.clear();
	needs.clear();
	score.clear();
	outcome.clear();
}

void ParseGoalDocs::ParseTaskFile(string inTaskFile)
{
	ifstream taskFile(inTaskFile);
	string str = "";
	while (taskFile >> str) {
		if (taskFlag) {
			if (FindWord(str, "<Goal>")) {
				goal.push_back(RemoveTag(str, "<Goal>"));
			}
			else if (FindWord(str, "<Need>")) {
				needsTemp.push_back(RemoveTag(str, "<Need>"));
			}
			else if (FindWord(str, "<Outcome>")) {
				outcome.push_back(RemoveTag(str, "<Outcome>"));
			}
			else if (FindWord(str, "<TaskEnd>")) {
				needs.push_back(needsTemp);
				needsTemp.clear();
				score.push_back(-10);
				taskFlag = false;
			}
		}
		else if (!taskFlag && FindWord(str, "<Task>")) {
			taskFlag = true;

		}
	}

}

void ParseGoalDocs::ParseScoreFile(string inScoresFile)
{
	string str = "";
	int goalNum = 0;
	ifstream scoreFile(inScoresFile);
	while (scoreFile >> str) {
		//Get task
		if (scorefilePass == 1) {
			for (int a = 0; a < goal.size(); a++) {
				if (goal[a] == str) {
					goalNum = a;
					scorefilePass++;
					break;
				}
			}
		}
		//Get score
		else if (scorefilePass == 3) {
			if (str == "%") {
				score[goalNum] = -1;
			}
			else {
				score[goalNum] = stoi(str);
			}
			scorefilePass = 1;
		}
		//Get "=" sign | skip over "="
		else {
			scorefilePass++;
		}	
	}
	scoreFileModTime = GetFileModTime(inScoresFile);
}

void ParseGoalDocs::ReturnVectors(vector<string>& goal, vector<vector<string>>& needs, vector<int>& score, vector<string>& outcome)
{
	goal = this->goal;
	needs = this->needs;
	score = this->score;
	outcome = this->outcome;
}