#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <sys\stat.h>

using namespace std;

class ParseGoalDocs
{
private:
	//Flags
	bool taskFlag = false;
	int scorefilePass = 1;
	int scoreFileModTime = 0;

	vector<string> goal;
	vector<vector<string>> needs;
	vector<string> needsTemp;
	vector<int> score;
	vector<string> outcome;

	bool FindWord(string line, string s) { return line.find(s) != std::string::npos; }
	string RemoveTag(string line, string tag);
	int GetFileModTime(string file);

public:
	ParseGoalDocs() {}
	~ParseGoalDocs() {}

	bool IfCurrentScoreFile(string inScoresFile);
	void ParseTaskFile(string inTaskFile);
	void ParseScoreFile(string inScoresFile);
	void ReturnVectors(
		vector<string>& goal,
		vector<vector<string>>& needs,
		vector<int>& score,
		vector<string>& outcome
	);
	void Clear();
};

