#pragma once
#include <TL-Engine.h>	
#include <vector>
#include <time.h>
#ifndef PATHFINDING_H
#define PATHFINDING_H
#include "PathFinding.h"
#endif
#ifndef GOAP_H
#define GOAP_H
#include "GOAP.h"
#endif
#ifndef WORLDGRID_H
#define WORLDGRID_H
#include "WorldGrid.h"
#endif
#ifndef PLACEABLEOJBJECTS_H
#define PLACEABLEOJBJECTS_H
#include "PlaceableObject.h"
#endif
#ifndef UICLASS_H
#define UICLASS_H
#include "UIClass.h"
#endif 
#ifndef GAMEOBJECTS_H
#define GAMEOBJECTS_H
#include "GameObjects.h"
#endif

using namespace tle;

class Agent
{
private:
	//This class stuff
	I3DEngine* tL;
	PathFinding* pathFinding;
	GOAP* goap;
	WorldGrid* wG;
	PlaceableObject* placeableObjects;
	UIClass* uiClass;

	enum States
	{
		Idle, 
		Walking, 
		GetTask, 
		Drinking, 
		Eating,
		NextTask,
		CutWood,
		Place_House_Site,
		Mining,
		Chopping,
		Build_House,
		Work,
		Sleep,
		Blank
	};

	enum LifeState
	{
		Alive,
		Dead,
		Zombie
	};

	int lifeState = Alive;

	IMesh* cubeMesh;
	IMesh* agentMesh;

	//Game information
	int id;
	int atXGrid;
	int atZGrid;
	IModel* model;

	const int Z_DIR[5] = { 1, 0, -1, 0, 0 }; //Manhattan heuristic
	const int X_DIR[5] = { 0, 1, 0, -1, 0 };

	//Agent information
	States currentState = States::Idle;
	int health = 100;
	int tillDeath = 20;
	float hungryLevel = 100.0f;
	float energyLevel = 100.0f;
	float thirstLevel = 100.0f;
	float needTimer = 0.0f;
	float actionTimer = 0.0f;
	float SPEED = 10.0f;
	float collectionTimer = 0.0f;
	float buildTimer = 0.0f;
	const float scale = 5.0f;

	float needToBuildHouse = 0.0f;

	float hungryDeceaseSpeed = 00.0f;
	float energyDeceaseSpeed = 0.0f;
	float thirstDeceaseSpeed = 00.0f;

	bool working = false;
	int holdingStone = 0;
	int holdingWood = 0;

	int houseId = -1;
	int size = 3;

	//Path finding
	vector<pair<float, float>> path;
	vector<pair<float, float>> shortPath;
	vector<pair<int, int>> shortPathInt;
	vector<IModel*> cubePath;
	int currentWaypoint = 0;
	bool UpdateWalking(float frameTime);
	bool CreatePath(int x, int z);
	void DeletePath(IMesh* mesh);

	//Tasks
	vector<string> tasks;
	int currentTask = 0;
	void GetWalkingLocationFromTask(string str, int&x, int& z);

	bool Collect(float frameTime);
	bool Build(float frameTime);

	//House
	vector<IModel*> housePath;
	void RemovePathObjects();
	void AddObjectPath(vector<pair<int, int>> path1);
	void AddBuilding(Objects obj, int x, int z, vector<pair<int, int>> path1);
	States UpdateState();
	int house = -1;

public:
	//Class methods
	Agent(I3DEngine* tL, WorldGrid* worldGrid, PlaceableObject* placeableObjects, UIClass* uiClass, IMesh* agentMesh, int id, float x, float y, float z, int gridX, int gridZ);
	~Agent();
	void LoadComponents(WorldGrid* wg);

	//Game methods
	void Update(float frameTime);
	void ShowPath(IMesh* mesh);
	void HidePath(IMesh* mesh);
	void PlaceBuilding(Objects obj);

	//Position methods
	float GetXPos() { return model->GetX(); }
	float GetZPos() { return model->GetZ(); }
	int GetGridXPos() { return atXGrid; }
	int GetGridZPos() { return atZGrid; }

	//Character methods
	int GetID() { return id; }
	void ChangeSkin(string skinPath) { model->SetSkin(skinPath, 0); }
	void GiveMenuParas(
		int statSpeedDeceaseMin,
		int statSpeedDeceaseMax,
		int agentLife);
	string GetStatus();
	int GetHealth() { return health; }
	int GetEnergy() { return energyLevel; }
	int GetHunger() { return hungryLevel; }
	int GetThirst() { return thirstLevel; }
	int GetWood() { return holdingWood; }
	int GetStone() { return holdingStone; }
	vector<string> GetTasks();

	void Kill() { lifeState = Dead; model->SetY(-20.0f); }
	void TurnToZombie() { lifeState = Zombie; SPEED = 30.0f; }

	
};

