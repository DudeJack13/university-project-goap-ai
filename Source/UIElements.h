#include <TL-Engine.h>

using namespace tle;

struct BaseOptions
{
	int x;
	int y;
	int spriteW;
	int spriteH;
	ISprite* sprite;

	bool Selected(int mouseX, int mouseY) {
		if (mouseX > sprite->GetX() && mouseX < sprite->GetX() + spriteW &&
			mouseY > sprite->GetY() && mouseY < sprite->GetY() + spriteH) {
			return true;
		}
		else {
			return false;
		}
	}
};

struct OptionBox : BaseOptions
{
	OptionBox(ISprite* s, string label, int x, int y, int spriteW, int spriteH) {
		sprite = s;
		this->label = label;
		this->x = x;
		this->y = y;
		this->spriteH = spriteH;
		this->spriteW = spriteW;
	}

	string label = "";
	string inputText = "";


	int GetValue() {
		if (inputText != "") {
			return stoi(inputText);
		}
		return -1;
	}
};

struct Button : BaseOptions
{
	Button(ISprite* s, string firstSkin, string secondSkin, int spriteW, int spriteH) {
		this->spriteH = spriteH;
		this->spriteW = spriteW;
		this->firstSkin = firstSkin;
		this->secondSkin = secondSkin;
		sprite = s;
	}

	void HoverUpdate(I3DEngine* tL, int mouseX, int mouseY) {
		if (mouseX > sprite->GetX() && mouseX < sprite->GetX() + spriteW &&
			mouseY > sprite->GetY() && mouseY < sprite->GetY() + spriteH) {
			if (!hover) {
				ChangeState(tL);
			}
			hover = true;
		}
		else {
			if (hover) {
				ChangeState(tL);
			}
			hover = false;
		}
	}

	bool IsHovered() { return hover; }

private:
	bool hover = false;
	bool block = false;
	string firstSkin;
	string secondSkin;

	void ChangeState(I3DEngine* tL) {
		ISprite* temp;
		if (!hover) {
			temp = tL->CreateSprite(secondSkin,
				sprite->GetX(), sprite->GetY());
		}
		else if (hover) {
			temp = tL->CreateSprite(firstSkin,
				sprite->GetX(), sprite->GetY());
		}
		tL->RemoveSprite(sprite);
		sprite = temp;
	}
};
