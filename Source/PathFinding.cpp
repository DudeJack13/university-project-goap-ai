#include "PathFinding.h"

PathFinding::PathFinding(WorldGrid* wg)
{
	worldGrid = wg;
}

vector<pair<int, int>> PathFinding::GetPath(vector<int> startCoords, vector<int> endCoords)
{
	if (startCoords[0] == endCoords[0] && startCoords[1] == endCoords[1]) {
		return vector<pair<int, int>>{ make_pair(startCoords[0], startCoords[1])};
	}
	//Set up
	deque <unique_ptr < Node > > openList; //Hold nodes the check
	deque <unique_ptr < Node > > closedList; //Hold checked nodes
	unique_ptr <Node> current; //Hold node currently getting checked from openlist
	unique_ptr <Node> node; //Hold node being created
	node.reset(new Node(startCoords[0], startCoords[1]));
	current = move(node);
	openList.push_back(move(current));

	while (!(openList.empty())) {
		sort(openList.begin(), openList.end(), CompareCoords()); //Sort the openlist
		current = move(openList.front()); //Move the front node from openlist into current
		openList.pop_front(); //Remove from openlist
		if (current->x == endCoords[0] && current->z == endCoords[1] && !repeat) { //If current node is end goal
			closedList.push_back(move(current));//push current to closed list
			vector<pair<int, int>> path;
			int x = closedList[closedList.size() - 1]->parent->x;
			int z = closedList[closedList.size() - 1]->parent->z;
			path.push_back(make_pair(closedList.back()->x, closedList.back()->z));
			for (int a = closedList.size() - 1; a > 0; a--) {
				if (closedList[a]->x == x && closedList[a]->z == z) {
					path.push_back(make_pair(closedList[a]->x, closedList[a]->z));
					x = closedList[a]->parent->x;
					z = closedList[a]->parent->z;
				}
			}
			return path; //Return found path
		}
		else {
			for (int a = 0; a < 4; a++) {
				node.reset(new Node(current->x + X_DIR[a], current->z + Y_DIR[a]));
				if (node->x < 0) {
					int h = 0;
				}
				//Check grid
				if ((node->x > -1 && node->x < worldGrid->GetXLenght() && node->z > -1 && node->z < worldGrid->GetZLenght())) {
					node->score = worldGrid->GetScore(node->x,node->z);
				}
				else {
					break;
				}
				//Check to see if its on the closed list already
				for (int b = 0; b < closedList.size(); b++) {
					if (node->x == closedList[b]->x && node->z == closedList[b]->z) {//If it is, skip it
						repeat = true;
						break;
					}
				}
				if (!repeat) { //If not on closed list
					if ((worldGrid->GetTaken(node->x, node->z) == -1 || 
						worldGrid->GetTaken(node->x, node->z) == Road ||
						worldGrid->GetTaken(node->x, node->z) == Tree ||
						node->x == endCoords[0] && node->z == endCoords[1])) {
						for (int b = 0; b < openList.size(); b++) //Check to make sure the node isnt on the openlist already
						{
							if (node->x == openList[b]->x && node->z == openList[b]->z) //If so, skip it...
							{
								repeat = true;
								break;
							}
						}
						if (!repeat) {
							node->HCost = (abs((endCoords[1] - node->z)) + (abs(endCoords[0] - node->x))); //Work out the H cost of the node
							node->GCost += node->score + current->GCost; //Work out the G cost of the node
							node->FCost = node->GCost + node->HCost; //Add them together
							node->parent = move(current.get()); //Sent the parent of the node to the current node being checked
							openList.push_back(move(node)); //Move node to open list
						}
						else {
							repeat = false;
						}
					}
					else {
						int affefew = 0;
					}
				}
				else repeat = false;
			}
			//After fully searching node
			closedList.push_back(move(current)); //and push to closed list
		}
	}
	return vector<pair<int, int>>();
}

vector<pair<float, float>> PathFinding::ConvertPath(vector<pair<int, int>> path)
{
	vector<pair<float, float>> floatPath;
	for (int a = path.size() - 1; a >= 0 ; a--) {
		floatPath.push_back(
			make_pair(
				worldGrid->GetGridPosX(path[a].first, path[a].second), 
				worldGrid->GetGridPosZ(path[a].first, path[a].second)
			)
		);
	}
	return floatPath;
}

vector<pair<int, int>> PathFinding::ConvertPathInt(vector<pair<int, int>> path)
{
	vector<pair<int, int>> intPath;
	for (int a = path.size() - 1; a >= 0; a--) {
		intPath.push_back(path[a]);
	}
	return intPath;
}

vector<pair<int, int>> PathFinding::RemoveWaste(vector<pair<int, int>> path)
{
	vector<pair<int, int>> returnPath{ path.front() };
	for (int a = 1; a < path.size() - 1; a++) {
		if ((path[a].first != path[a - 1].first && path[a].second != path[a + 1].second) ||
			(path[a].first != path[a + 1].first && path[a].second != path[a - 1].second)) {
			returnPath.push_back(path[a]);
		}
	}
	returnPath.push_back(path.back());
	return returnPath;
}

PathFinding::Node::Node()
{
	x = 0;
	z = 0;
	score = 0;
	GCost = 0;
	HCost = 0;
	FCost = 0;
}