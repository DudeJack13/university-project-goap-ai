#pragma once
#include <deque>
#include <algorithm>
#include <vector>
#include <memory>
#ifndef WORLDGRID_H
#define WORLDGRID_H
#include "WorldGrid.h"
#endif
#ifndef GAMEOBJECTS_H
#define GAMEOBJECTS_H
#include "GameObjects.h"
#endif
using namespace std;

class PathFinding
{
private:

	WorldGrid* worldGrid;
	const int Y_DIR[4] = { 1, 0, -1, 0 }; //Manhattan heuristic
	const int X_DIR[4] = { 0, 1, 0, -1 };

	bool repeat = false;

	struct Node
	{
		int x; 
		int z;
		int score= -1; //The score from the inported map
		int GCost;//How far its travelled (cost too)
		int HCost;//How far to the end 
		int FCost;//G + H
		Node* parent;//The parent node
		Node();
		Node(int _x, int _z) {
			x = _x; z = _z; //Set grid x and y
		}
	};
	struct CompareCoords
	{
		inline bool operator() (unique_ptr<Node>& lhs, unique_ptr<Node>& rhs) {
			return lhs->FCost < rhs->FCost;
		}
	};
public:
	PathFinding(WorldGrid* wg);
	~PathFinding() {}

	vector<pair<int, int>> GetPath(vector<int> startCoord, vector<int> endCoord);
	vector<pair<float, float>> ConvertPath(vector<pair<int, int>> path);
	vector<pair<int, int>> RemoveWaste(vector<pair<int, int>> path);
	vector<pair<int, int>> ConvertPathInt(vector<pair<int, int>> path);
};


