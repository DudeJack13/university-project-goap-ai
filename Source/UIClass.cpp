#include "UIClass.h"

UIClass::UIClass(I3DEngine* tL)
{
	this->tL = tL;
}

void UIClass::LoadGameUI()
{
	bottomY = tL->GetHeight();
	yIcon = bottomY - 53;
	font4 = tL->LoadFont("Font1.bmp", 25);
	font3 = tL->LoadFont("Font1.bmp", 44);
	font2 = tL->LoadFont("Font1.bmp", 34);
	font1 = tL->LoadFont("Font1.bmp", 24);
	uiSelector = tL->CreateSprite("uiSelector.png", -160.0f, bottomY - 320, 0.0f);
	arrowIcon_off = tL->CreateSprite("arrowIcon_off.png", xIcon, bottomY - 53, 0.0f);
	arrowIcon_on = tL->CreateSprite("arrowIcon_on.png", xIcon, bottomY, 0.0f);
	pathIcon_off = tL->CreateSprite("pathIcon_off.png", xIcon + 59, bottomY - 53, 0.0f);
	pathIcon_on = tL->CreateSprite("pathIcon_on.png", xIcon + 59, bottomY, 0.0f);
	buttonFrame = tL->CreateSprite("ButtonOutline.png", 532.0f, bottomY - 59, 0.0f);
}

void UIClass::LoadLoadingUI()
{
	outputFont = font1 = tL->LoadFont("Font1.bmp", 15);
	displayWidth = tL->GetWidth();
	displayHeight = tL->GetHeight();
}

void UIClass::OutputToConsoleScreen(string str)
{
	if (displayText.size() >= MAX_MESSAGES_SHOWN) {
		displayText.erase(displayText.begin());
	}
	for (int a = 0; a < displayText.size(); a++) {
		displayText[a].y -= DISANCE_BETWEEN_LINES;
	}
	displayText.push_back(DisplayText(str, displayWidth - 10, displayHeight - 50));
}

void UIClass::DrawOutputUI(float frameTime)
{
	int loop = 0;
	if (displayText.size() >= MAX_MESSAGES_SHOWN) {
		loop = MAX_MESSAGES_SHOWN;
	}
	else {
		loop = displayText.size();
	}
	for (int a = 0; a < loop; a++) {
		outputFont->Draw(displayText[a].text, displayText[a].x, displayText[a].y, kBlack, kRight);
		displayText[a].timerCurrent += COUNTER_TIME * frameTime;
		if (!displayText[a].dead && displayText[a].timerCurrent > TIMER_MAX) {
			displayText[a].text = "";
			displayText[a].dead = true;
		}
	}
}

void UIClass::DrawDebug(float frameTime)
{
	float fps = 1.0f / frameTime;
	//string text = 
	//	"FPS: " + to_string(fps) + 
	//	"\nFrameTime: " + to_string(frameTime) +
	//	"\nMouse X: " + to_string(tL->GetMouseX()) +
	//	"\nMouse Y: " + to_string(tL->GetMouseY()) +
	//	"\nMouse Scroll: " + to_string(tL->GetMouseWheel()) +
	//	"\nmouseInUI: " + to_string(mouseInUI) +
	//	"\ncurrentID: " + to_string(currentId);
	string text = "FPS: " + to_string(fps);
	outputFont->Draw(text, 10, 10);
}

void UIClass::DrawText(string s, int x, int y)
{
	font1->Draw(s, x, y);
}

void UIClass::UpdateMouse()
{
	if (tL->GetMouseX() <= 517.0f && tL->GetMouseY() >= bottomY - 320) {
		mouseInUI = true;
	}
	else {
		mouseInUI = false;
	}
}

bool UIClass::GetInUIState()
{
	return mouseInUI;
}

void UIClass::AddToList(int i, string s)
{
	text.push_back(make_pair(i, s));
}

int UIClass::GetCurrentSelected()
{
	return currentId;
}

void UIClass::RemoveFromList(int i)
{
	vector<pair<int, string>> temp = text;
	text.clear();
	for (int a = 0; a < temp.size(); a++) {
		if (temp[a].first != i) {
			text.push_back(make_pair(temp[a].first, temp[a].second));
		}
	}
	
}

void UIClass::ScrollInList(int scrollValue)
{
	if (tL->KeyHit(tle::Key_Up)) {
		currentId--;
	}
	else if (tL->KeyHit(tle::Key_Down)) {
		currentId++;
	}
	else {
		currentId -= scrollValue;
	}
	int c = text.size();
	if (currentId >= c) {
		currentId = text.size() - 1;
	}
	else if (currentId < 0) {
		currentId = 0;
	}
}

void UIClass::PrintText()
{
	if (!text.empty()) {
		if (currentId - 2 >= 0) {
			font1->Draw(text[currentId - 2].second, selectionX, bottomY - 150);
		}
		if (currentId - 1 >= 0) {
			font2->Draw(text[currentId - 1].second, selectionX, bottomY - 130);
		}
		if (currentId >= 0) {
			font3->Draw("< " + text[currentId].second, selectionX - 15, bottomY - 100);
		}
		if (currentId + 1 < text.size()) {
			font2->Draw(text[currentId + 1].second, selectionX, bottomY - 60);
		}
		if (currentId + 2 < text.size()) {
			font1->Draw(text[currentId + 2].second, selectionX, bottomY - 30);
		}
	}
}

void UIClass::PrintAIInformation(string s, int h, int e, int t, int he, int wood, int stone)
{
	if (s != "") {
		font2->Draw("Status: " + s, 10, bottomY - 150);
		font4->Draw("Hungry: " + to_string(h) + "%", 10, bottomY - 120);
		font4->Draw("Energy: " + to_string(e) + "%", 10, bottomY -100);
		font4->Draw("Thirst: " + to_string(t) + "%", 10, bottomY - 80);
		font4->Draw("Health: " + to_string(he) + "%", 170, bottomY - 120);

		font4->Draw("Holding Wood: " + to_string(wood) + "%", 10, bottomY - 50);
		font4->Draw("Holding Stone: " + to_string(stone) + "%", 10, bottomY - 30);
	}
}

void UIClass::UIButtons(bool& arrow, bool& path)
{
	if ((tL->GetMouseX() >= xIcon && tL->GetMouseX() <= xIcon + 53) && tL->GetMouseY() >= yIcon) {
		if (!arrowShow) {
			arrowShow = true;
			arrowIcon_off->SetY(-100.0f);
			arrowIcon_on->SetY(bottomY - 53);
		}
		else if (arrowShow) {
			arrowShow = false;
			arrowIcon_off->SetY(bottomY - 53);
			arrowIcon_on->SetY(-100.0f);
		}
	}
	else if ((tL->GetMouseX() >= xIcon + 59 && tL->GetMouseX() <= xIcon + 112) && tL->GetMouseY() >= yIcon) {
		if (!pathShow) {
			pathShow = true;
			pathIcon_off->SetY(-100.0f);
			pathIcon_on->SetY(bottomY - 53);
		}
		else if (pathShow) {
			pathShow = false;
			pathIcon_off->SetY(bottomY - 53);
			pathIcon_on->SetY(-100.0f);
		}
	}
	path = pathShow;
	arrow = arrowShow;
}

void UIClass::UpdateTaskList(vector<string> s)
{
	int x = 5;
	int base = 275;
	font2->Draw("Task List ", x, bottomY - 300);
	for (int a = 0; a < s.size(); a++) {
		font1->Draw(s[a], x, (bottomY - 275) + (a * 20));
	}
}

