#include "GOAP.h"

GOAP::GOAP(WorldGrid* worldGrid, PathFinding* pathFinding, PlaceableObject* placeableObjects)
{
	this->worldGrid = worldGrid;
	this->pathFinding = pathFinding;
	this->placeableObjects = placeableObjects;
	parseGoalDocs = new ParseGoalDocs();
	parseGoalDocs->ParseTaskFile(".\\Goals\\Tasks.txt");
	parseGoalDocs->ParseScoreFile(".\\Goals\\Scores.txt");
	vector<string> g;
	vector<vector<string>> pC;
	vector<int> s;
	vector<string> o;
	parseGoalDocs->ReturnVectors(g, pC, s, o);
	for (int a = 0; a < g.size(); a++) {
		tasks.push_back(new Tasks(g[a], pC[a], s[a], o[a]));
	}
	parseGoalDocs->Clear();
}

void GOAP::CheckScoresUpdate()
{
	if (parseGoalDocs->IfCurrentScoreFile(".\\Goals\\Scores.txt")) {
		parseGoalDocs->ParseScoreFile(".\\Goals\\Scores.txt");
	}
}

bool GOAP::ValidatedIfReal(string object)
{
	//object = RemoveTag(object);
	if (object == "Drink_At_Well") {
		return placeableObjects->GetAmountOfObject(Well) > 0;
	}
	else if (object == "Eat_At_KFC") {
		return placeableObjects->GetAmountOfObject(KFC) > 0;
	}
	else if (object == "Place_To_Sleep") {
		return agentHouseID != -1;
	}
	else if (object == "Chop_Tree") {
		return placeableObjects->GetAmountOfObject(Tree) > 0;
	}
	else if (object == "Mine_Stone") {
		return placeableObjects->GetAmountOfObject(Rock) > 0;
	}
	else if (object == "Construct_House") {
		return placeableObjects->GetAmountOfObject(House) > 0;
	}
	return false;
}

string GOAP::RemoveTag(string object)
{
	string tag = "GameItem ";
	int tagLenght = tag.length();
	string returnString = "";
	for (char& c : object) {
		if (tagLenght > 0) {
			tagLenght--;
		}
		else {
			returnString += c;
		}
	}
	return returnString;
}

int GOAP::GetMoveScore(string locationName, int gridXStart, int gridZStart, string& returnGoal)
{
	int x = 0;
	int z = 0;
	string str = "";
	vector<pair<int, int>>  p;
	if (locationName == "Drink_At_Well") {
		 p = GetClosesItem("Well", gridXStart, gridZStart, str);
	}
	else if (locationName == "Eat_At_KFC") {
		p = GetClosesItem("Food", gridXStart, gridZStart, str);
	}
	else if (locationName == "Place_To_Sleep") {
		p = GetClosesItem("House", gridXStart, gridZStart, str);
	}
	else if (locationName == "Place_House_Site") {
		p = GetClosesItem("Empty", gridXStart, gridZStart, str);
	}
	else if (locationName == "Mine_Stone") {
		p = GetClosesItem("Stone", gridXStart, gridZStart, str);
	}
	else if (locationName == "Chop_Tree") {
		p = GetClosesItem("Wood", gridXStart, gridZStart, str);
	}
	else if (locationName == "Construct_House") {
		int houseX = 0;
		int houseZ = 0;
		placeableObjects->GetLocationOfObject(House, agentHouseID, houseX, houseZ);
		returnGoal = "Move(" + to_string(houseX) + "," + to_string(houseZ) + ")";
		return 0;
	}
	returnGoal = str;
	return p.size();
}

bool GOAP::CheckAgentInventory(string task)
{
	if (task == "Mine_Stone") {
		return agentStone > 0;
	}
	else if (task == "Chop_Tree") {
		return agentWood > 0;
	}
	return false;
}

void GOAP::CreateTrees(vector<string> taskNames)
{
	vector<Tasks*> needToCheck;
	//Get tasks that outcome matches goal
	for (int c = 0; c < taskNames.size(); c++) {
		for (int a = 0; a < tasks.size(); a++) {
			if (tasks[a]->goal == taskNames[c]) {
				tree.push_back(new Tasks(tasks[a]->goal, tasks[a]->needs, tasks[a]->score, tasks[a]->outcome));
				needToCheck.push_back(tree.back());
				break;
			}
		}

		//Connect the tree based on the goal found above

		while (!needToCheck.empty()) {
			//Check pre tasks from front of list
			for (int a = 0; a < needToCheck.front()->needs.size(); a++) { //NeedToCheck tasks loop
				for (int b = 0; b < tasks.size(); b++) {
					//Find if the need of the tasks matches the outcome
					if (needToCheck.front()->needs[a] == tasks[b]->outcome) {
						Tasks* temp = new Tasks(tasks[b]->goal, tasks[b]->needs, tasks[b]->score, tasks[b]->outcome);
						temp->parent = needToCheck.front();
						needToCheck.push_back(temp);
						needToCheck.front()->childTask.push_back(temp);
					}
				}
			}
			needToCheck.erase(needToCheck.begin());
		}
	}
	needToCheck.clear();
}

vector<string> GOAP::GetTasks(string goal, int gridXStart, int gridZStart)
{
	//Find goal
	Tasks* goalTree; //Pointer to goal tree
	bool found = false;
	for (int a = 0; a < tasks.size(); a++) {
		if (tree[a]->goal == goal) {
			goalTree = tree[a];
			found = true;
			break;
		}
	}
	if (!found) {
		return vector<string>{"Error, goal not found"};
	}

	//Find best tasks via score

	vector<Tasks*> toCheck; //Vector to hold all tasks that need checking
	toCheck.push_back(goalTree); //Adding start tasks

	//While there are tasks to check
	while (toCheck.size() > 0) {

		//Sort the toCheckList
		sort(toCheck.begin(), toCheck.end(), CompareTasks());

		//Setting checking from toCheck list, should be lowest currentscore
		Tasks* checking = toCheck.front();
		toCheck.erase(toCheck.begin());

		//check the children
		if (checking->childTask.size() > 0) {
			for (int a = 0; a < checking->childTask.size(); a++) {
				//Adding child to be checked
				//Check agent resources
				if (!CheckAgentInventory(checking->goal)) {
					if (ValidatedIfReal(checking->childTask[a]->goal) || (checking->childTask[a]->goal.find("Move") != std::string::npos)) {
						toCheck.push_back(checking->childTask[a]);
						//Updated the score of the child
						if (toCheck.back()->goal == "Move") {
							toCheck.back()->currentScore = checking->score + GetMoveScore(toCheck.back()->parent->goal, gridXStart, gridZStart, toCheck.back()->goal);
						}
						else {
							toCheck.back()->currentScore = toCheck.back()->score + checking->score;
						}
					}
				}
			}
		}
		else {
			//Return tasks in a string vector
			vector<string> returnString;
			while (checking->goal != goal) {
				returnString.push_back(checking->goal);
				checking = checking->parent;
			}
			return returnString;
		}
	}
	return vector<string>{"No tasks found"};
}

vector<pair<int, int>> GOAP::GetClosesItem(string item, int gridXStart, int gridZStart, string& returnGoal)
{
	int bestX = 0;
	int bestZ = 0;
	int x = 0;
	int z = 0;
	int bestIndex = -1;
	int bestScore = 100000;
	vector<pair<int, int>> temp;
	vector<pair<int, int>> bestPath;
	Objects obj;
	if (item != "Empty") {
		if (item == "House") {
			placeableObjects->GetLocationOfObject(House, agentHouseID, x, z);
			temp = pathFinding->GetPath(vector<int>{gridXStart, gridZStart}, vector<int>{x, z});
			returnGoal = "Move(" + to_string(x) + "," + to_string(z) + ")";
			return bestPath;
		}
		else if (item == "Well") { obj = Well; }
		else if (item == "Food") { obj = KFC; }
		else if (item == "Stone") { 
			obj = Rock;
		}
		else if (item == "Wood") {
			obj = Tree;
		}
		for (int b = 0; b < placeableObjects->GetAmountOfObject(obj); b++) {
			placeableObjects->GetLocationOfObject(obj, b, x, z);
			temp = pathFinding->GetPath(vector<int>{gridXStart, gridZStart}, vector<int>{x, z});
			if (temp.size() < bestScore) {
				bestX = x;
				bestZ = z;
				bestIndex = b;
				bestScore = temp.size();
				bestPath = temp;
			}
			if (bestScore < 10) {
				break;
			}
		}
	}
	returnGoal = "Move(" + to_string(bestX) + "," + to_string(bestZ) + ")";
	return bestPath;
}

void GOAP::GiveAgentInventory(int wood, int stone, int state)
{
	agentWood = wood;
	agentStone = stone;
	agentState = state;
}

GOAP::~GOAP()
{
	pathFinding->~PathFinding();
	for (int a = 0; a < tree.size(); a++) {
		delete(tree[a]);
	}
	tree.clear();
	for (int a = 0; a < tasks.size(); a++) {
		if (tasks[a] != nullptr) {
			delete(tasks[a]);
		}
	}
	tasks.clear();
}


