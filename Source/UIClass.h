#pragma once
#include <TL-Engine.h>	
#include <vector>
using namespace tle;

class UIClass
{
private:
	//Engine
	I3DEngine* tL;
	//Loading/Console
	IFont* outputFont;
	struct DisplayText
	{
		DisplayText(string text, int x, int y) {
			this->text = text;
			this->x = x;
			this->y = y;
		}
		float timerCurrent = 0.0f;
		string text = "";
		int x = 0;
		int y = 0;
		bool dead = false;
	};
	vector <DisplayText> displayText;
	int displayWidth = 0;
	int displayHeight = 0;
	const int MAX_MESSAGES_SHOWN = 20;
	const int DISANCE_BETWEEN_LINES = 13;
	const float COUNTER_TIME = 1.0f;
	const float TIMER_MAX = 7.5f;
	//Game
	IFont* font1;
	IFont* font2;
	IFont* font3;
	IFont* font4;

	ISprite* uiSelector;

	ISprite* buttonFrame;
	ISprite* arrowIcon_off;
	ISprite* arrowIcon_on;
	ISprite* pathIcon_off;
	ISprite* pathIcon_on;

	const float xIcon = 538.0f;
	float yIcon = 0.0f;

	float bottomY = 0.0f;

	const int selectionX = 330;
	const int y = 567;
	const int yOffset = 15;
	vector<pair<int, string>> text;

	bool mouseInUI = false;
	int currentId = 0;

	bool arrowShow = false;
	bool pathShow = false;
	

public:
	UIClass(I3DEngine*);
	~UIClass() {}

	void DrawText(string s, int x, int y);
	void UpdateMouse();
	bool GetInUIState();
	void AddToList(int id, string s);
	void RemoveFromList(int id);
	int GetCurrentSelected();
	void ScrollInList(int scrollValue);
	void PrintText();
	void PrintAIInformation(string s, int hungry, int energy, int thirst, int health, int wood, int stone);
	void UIButtons(bool& arrow, bool& path);
	void UpdateTaskList(vector<string>);
	void LoadLoadingUI();
	void LoadGameUI();
	void OutputToConsoleScreen(string str);
	void DrawOutputUI(float frameTime);
	void DrawDebug(float frameTime);
};

